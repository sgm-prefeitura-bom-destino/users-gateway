import { BadRequestException, Body, Controller, Logger, Post, Put, Req, UnauthorizedException } from '@nestjs/common';
import { Request } from 'express';
import { UserDaoService } from 'src/database/user-dao/user-dao.service';
import { User } from 'src/domain/user';

@Controller('user')
export class UserController {

  constructor(private userDao: UserDaoService) { }

  @Post()
  async save(@Body() user: User): Promise<User> {
    return this.userDao.save(user).catch((e) => {
      throw new BadRequestException(e)
    });
  }

  @Put()
  async update(@Body() user : User, @Req() req : Request) : Promise<User> {
    const result = await this.userDao.verifyPassword(user);
    const reqUser = req.user as any;

    if (!result || reqUser.email !== user.email) {
      throw new BadRequestException(null, "Usuário ou senha inválidos");
    }

    return this.userDao.save(user).catch(() => {
      throw new BadRequestException();
    })

  }

  @Post("/login")
  async login(@Body() user: User): Promise<User> {
    const result = await this.userDao.verifyPassword(user);

    if (!result) {
      throw new UnauthorizedException(null, "Usuário ou senha inválidos");
    }

    return result;
  }

}
