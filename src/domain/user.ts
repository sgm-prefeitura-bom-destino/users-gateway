import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({ timestamps: true, toObject: { virtuals: true } })
export class UserEntity extends Document {

  static schemaName = "User";

  @Prop({ required: true, unique: true })
  email: String
  
  @Prop({ required: true, unique: true })
  cpf: String

  @Prop({ required: true })
  password: String
  
  @Prop({ required: true })
  address: String

  @Prop({ required: true })
  name: String
}

export interface User {
  id?: String,
  email: String,
  password: String,
  name: String,
  cpf: String,
}

export const UserSchema = SchemaFactory.createForClass(UserEntity);