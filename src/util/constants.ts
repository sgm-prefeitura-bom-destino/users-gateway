export const Constants = {
  VAULT_SETTINGS: {
    endpoint: process.env.VAULT_URL || 'http://127.0.0.1:8200',
    token: process.env.VAULT_TOKEN || 'TOKEN_DEV'
  },
  SESSION_MAX_DAYS: 1,
  MONGO_URL: process.env.MONGO_URL || 'mongodb://localhost:27017/',
  MONGO_SETTINGS: {
    dbName: process.env.DATABASE || "sgm-users",
    auth: { user: process.env.MONGO_USER, password: process.env.MONGO_PSWD },
    useNewUrlParser: true, useFindAndModify: false, useCreateIndex: true
  },
}