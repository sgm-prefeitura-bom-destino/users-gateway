import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { DatabaseModule } from './database/database.module';
import { VaultModule } from './vault/vault.module';
import { UserController } from './user/user.controller';
import { JwtAuthGuard } from './auth/jwt-auth.guard';
import { APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import { UserInternalController } from './user-internal/user-internal.controller';
import { LoggingInterceptor } from './interceptors/logging.interceptor';

@Module({
  imports: [AuthModule, VaultModule, DatabaseModule],
  controllers: [UserController, UserInternalController],
  providers: [
    { provide: APP_GUARD, useClass: JwtAuthGuard },
    { provide: APP_INTERCEPTOR, useClass: LoggingInterceptor },
  ],
})
export class AppModule {}
