import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';

@Injectable()
export class EncryptorService {

  saltRounds = 12;

  async generateHash(plainPswd) : Promise<string> {
    const salt = await bcrypt.genSalt(this.saltRounds);
    return bcrypt.hash(plainPswd, salt);
  }

  async compare(plainPswd, hash) : Promise<boolean> {
    return bcrypt.compare(plainPswd, hash);
  }

}
