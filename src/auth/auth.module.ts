import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { VaultModule } from 'src/vault/vault.module';
import { JwtAuthGuard } from './jwt-auth.guard';
import { JwtModuleRegisterService } from './jwt-module-register/jwt-module-register.service';
import { JwtStrategy } from './jwt.strategy';
import { EncryptorService } from './encryptor/encryptor.service';

@Module({
  imports: [
    VaultModule,
    JwtModule.registerAsync({ useClass: JwtModuleRegisterService, imports: [VaultModule]  })
  ],
  providers: [JwtStrategy, JwtAuthGuard, EncryptorService],
  exports: [JwtAuthGuard, EncryptorService]
})
export class AuthModule { }
