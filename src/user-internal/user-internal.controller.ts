import { Body, Controller, HttpStatus, Post, Res, UnauthorizedException } from '@nestjs/common';
import { Response } from 'express';
import { UserInternal } from 'src/domain/user-internal';

@Controller('user-internal')
export class UserInternalController {

  @Post("/login")
  async login(@Body() user: UserInternal) {
    const { password, ...result } = matchLogin(user);
    if (!result) {
      throw new UnauthorizedException(null, "Usuário ou senha inválidos");
    }

    return result;
  }

}

const matchLogin = (user: UserInternal) => {
  return employees.find(a => a.matricula === user.matricula && a.password === user.password);
}

const employees = [
  { matricula: "00000-00", name: "Funcionario", password: "123456" },
  { matricula: "11111-11", name: "Funcionario 2", password: "password" },
]
