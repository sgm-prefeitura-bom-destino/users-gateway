import { Module } from '@nestjs/common';
import { VaultLoader } from './vault-loader/vault-provider.service';

@Module({
  providers: [VaultLoader],
  exports: [VaultLoader]
})
export class VaultModule {}
