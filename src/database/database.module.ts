import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from 'src/auth/auth.module';
import { UserEntity, UserSchema } from 'src/domain/user';
import { DatabaseRegisterService } from './database-register.service';
import { UserDaoService } from './user-dao/user-dao.service';

@Module({
  imports: [
    AuthModule,
    MongooseModule.forRootAsync({ useClass: DatabaseRegisterService }),
    MongooseModule.forFeature([
      { name: UserEntity.schemaName, schema: UserSchema },
    ])
  ],
  providers: [UserDaoService],
  exports: [UserDaoService]
})
export class DatabaseModule { }
