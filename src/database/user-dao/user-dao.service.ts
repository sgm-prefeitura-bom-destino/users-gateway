import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { EncryptorService } from 'src/auth/encryptor/encryptor.service';
import { User, UserEntity } from 'src/domain/user';
import { toDto } from 'src/util/toDto';

@Injectable()
export class UserDaoService {
  constructor(
    @InjectModel(UserEntity.schemaName) private readonly model: Model<UserEntity>,
    private readonly encryptor: EncryptorService) { }

  async save(execution: User): Promise<User> {
    if (execution.id) {
      const { password, ...newUser } = execution;
      return this.model.findOneAndUpdate({ email: execution.email }, newUser).then(toDto);
    } else {
      const hash = await this.encryptor.generateHash(execution.password);
      return new this.model({ ...execution, password: hash }).save().then(toDto);
    }
  }

  async verifyPassword(user: User): Promise<User> {
    const userEntity = await this.model.findOne({ email: user.email });

    if (userEntity) {
      const result = await this.encryptor.compare(user.password, userEntity?.password);
      if (result) return toDto(userEntity);
    }

    return null;
  }

  findAll(limit: number = 10): Promise<User[]> {
    return this.model.find({}).limit(limit).then(list => list.map(toDto));
  }

  find(id: string): Promise<User> {
    return this.model.findOne({ id }).then(toDto);
  }
}
