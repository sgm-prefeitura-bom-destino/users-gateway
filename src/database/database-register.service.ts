import { Injectable } from "@nestjs/common";
import { MongooseModuleOptions, MongooseOptionsFactory } from "@nestjs/mongoose";
import { Constants } from "src/util/constants";

@Injectable()
export class DatabaseRegisterService implements MongooseOptionsFactory {

  createMongooseOptions(): MongooseModuleOptions | Promise<MongooseModuleOptions> {
    const auth = Constants.MONGO_SETTINGS.auth?.user ? Constants.MONGO_SETTINGS.auth : null;
    return {
      uri: Constants.MONGO_URL,
      ...Constants.MONGO_SETTINGS,
      auth
    }
  }
  
}